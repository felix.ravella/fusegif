<?php 
include './functions.php';

$assets_dir = "./assets/";
$output_dir = "./output/";
$assets = array_values(array_filter(scandir($assets_dir), "is_image_file"));

if (isset($_POST["recipe"])) {
  $width = $_POST['width'];
  $height = $_POST['height'];


  $files = json_decode($_POST["recipe"]);
  $output_name = $_POST["output_name"];
  
  // recuperation des frames
  $frame_numbers = [];
  foreach($files as $file):
    $number = exec('identify -format "%n\n" ' . $assets_dir . $file[0] .' | head -1');
    array_push($frame_numbers, $number);
  endforeach;

  $commun_number = get_common_multiple($frame_numbers);
  //composition de la commande
  $command = "convert ${assets_dir}void.png -resize ${width}x${height} -extent ${width}x${height} "; 

  for ($i = 0; $i < sizeof($files); $i++ ) :
    $command.= "null: \( $assets_dir" . $files[$i][0] . " -coalesce ";
    $frames = $frame_numbers[$i];
    
    // gestion des repetitions des frames necessaires
    if ($frames < $commun_number) :
      $sequence = get_sequence($frames);
      $sequences_to_add = ($commun_number / $frames) -1; 

      $command .= "\( -clone ";
      for ($j = 0; $j < $sequences_to_add; $j++):
        $command .= ($j == $sequences_to_add -1) ? $sequence : $sequence . ',';
      endfor;
      $command .= " \) ";
    endif;

    // gestion de la position
    $command .= "\) -gravity NorthWest -geometry " . $files[$i][1] . " -layers Composite ";
  endfor;

  $command .= "${output_dir}${output_name}.gif";

  echo "commande: $command <br>";
  $output = array();
  print_r($output);

  exec($command,$output);
  
  header("Location: result.php?name=${output_name}");

}



?>
