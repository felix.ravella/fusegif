<?php

function get_primes($number){
  $i = 2;
  while ($number > 1) {
    if($number % $i == 0) {
      $primes[] = $i;
      $number = $number / $i;
    }
    else $i++;
  }
  return array_count_values($primes);
}

function get_common_multiple($numbers) {
  $arr = [];
  // recupération des facteurs premiers de plus grande puissance
  
  foreach ($numbers as $number):
    $primes = get_primes($number);
    foreach ($primes as $prime => $power):
      if (!isset($arr[$prime])):
        $arr[$prime] = $power;
      else:
        $arr[$prime] = ($arr[$prime] < $power) ? $power : $arr[$prime];
      endif;
    endforeach;
  endforeach;
  // calcul du ppcm
  $ppcm = 1;
  foreach ($arr as $prime => $power):
    $ppcm *= $prime ** $power;
  endforeach;
  return $ppcm;
}

function get_sequence($number){
  $sequence = "";
  for ($i = 0; $i < $number; $i++):
    $sequence .= ($i == $number -1) ? $i : $i . ',';
  endfor;
  return $sequence;
}

function is_image_file($str) {
  $file_type = substr($str, -4);
  return ($str != '.' && $str != '..' && ($file_type == ".gif" || $file_type == ".png") );
}

?>
