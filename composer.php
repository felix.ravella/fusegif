<?php
include './functions.php';

$assets_dir = "./assets/";
$assets = array_values(array_filter(scandir($assets_dir), "is_image_file"));
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="./style.css" rel="stylesheet">
  </head>
  <body>
  
    <div class="container">
    <!-- Fields de taille -->
      <div id="controls">
        <label for="width"> Largeur: </label>
        <input id="width" name="width" value=400 type=number step=1 onchange="changeWidth(this.value)"><br>
        
        <label for="height"> Hauteur: </label>
        <input id="height" name="height" value=400 type=number step=1 onchange="changeHeight(this.value)"><br>
        <br>

        <form id="form" method="POST" action="./generator.php">
          <label for="output-name"> Nom </label>
          <input id="output-name" type="text" placeholder="new-image">
        </form>

        <button onclick="save()"> Fusionner </button>
      </div>

      <!-- Liste des selecteurs -->
      <div>
        <ul id="gif-list">
        </ul>
        <button onclick="addSelect()"> + </button>
      </div>

      <!-- Zone de composition -->
      <div id="playground">
      </div>
    </div>

  </body>
</html>

<script lang="js">
  const coordsRegex = /translate3d\((?<x>.*?)px, (?<y>.*?)px, (?<z>.*?)px/;
  var playground = document.getElementById("playground");
  var draggedElement = null;

  playground.addEventListener("mousedown", dragStart, false);
  playground.addEventListener("mouseup", dragEnd, false);
  playground.addEventListener("mousemove", drag, false);

  var currentX;
  var currentY;
  var initialX;
  var initialY;

  var elements = [];
  var files = <?php echo json_encode($assets); ?>;
  console.log(files);
  var assets_dir = "<?php echo $assets_dir; ?>"; 
  
  function addSelect() {
    let e = document.getElementById("gif-list");
    elements.push("");
    var str = "";
    for (let i = 0; i < elements.length; i++) {
      let select_string = `<li><select id="${i}" name="select-${i}" onchange="selectChange(this.id, this.value)">`;
      select_string += '<option value="">--Choisir un fichier--</option>'
      for (file of files) {
        let is_selected = (elements[i] === file) ? "selected" : "";
        select_string += `<option value="${file}" ${is_selected}>${file}</option>`;
      }
      select_string += "</select></li>";
      str += select_string;
    }
    e.innerHTML = str;
  }

  function changeWidth(value) {
    playground.style.width = `${value}px`;
    canvasWidth = value;
  }

  function changeHeight(value) {
    playground.style.height = `${value}px`;
    canvasHeight = value;
  }

  function selectChange(id, value){
    elements[id] = value;
    let e = document.getElementById(`gif-${id}`);
    if (!e) {
      playground.innerHTML += `<img id="gif-${id}" class="gif" src="${assets_dir}${value}" style="z-index:${id}"></div>`;
    }
    else {
      e.src = `${assets_dir}${value}`;
    }
  }
  
  function dragStart(e) {
    xOffset = 0;
    yOffset = 0;
    if (!!e.target.style.transform) {
      let coords = coordsRegex.exec(e.target.style.transform);
      xOffset = coords.groups.x;
      yOffset = coords.groups.y;
    }

    initialX = e.clientX - xOffset;
    initialY = e.clientY - yOffset;
    if (e.target.id.substring(0,3) === "gif") {
      draggedElement = e.target;
    }
  }
  
  function dragEnd(e) {
    draggedElement = null;
  }

  function drag(e) {
    if (!!draggedElement) {
      e.preventDefault();

      currentX = e.clientX - initialX;
      currentY = e.clientY - initialY;

      draggedElement.style.transform = `translate3d(${currentX}px, ${currentY}px, 0)`;
    }
  }

  function save() {
    let outputName = document.getElementById("output-name").value;
    if (!outputName) {
      return;
    }
    
    let recipe = [];
    for (let i = 0; i < elements.length; i++) {
      let gif = document.getElementById(`gif-${i}`);
      let x = "+0";
      let y = "+0";
      if (gif.style.transform) {
        let coords = coordsRegex.exec(gif.style.transform);
        x = (coords.groups.x >= 0) ? "+" + coords.groups.x : "" + coords.groups.x; 
        y = (coords.groups.y >= 0) ? "+" + coords.groups.y : "" + coords.groups.y; 
      }
      recipe.push([elements[i], `${x}${y}`])
    }
    var data = JSON.stringify(recipe);
    // creation d'un form caché et envoi pour passer data en POST
    var form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action", "./generator.php");

    var dataField = document.createElement("input");
    dataField.setAttribute("type", "hidden");
    dataField.setAttribute("name", "recipe");
    dataField.setAttribute("value", data);

    var nameField = document.createElement("input");
    nameField.setAttribute("type", "hidden");
    nameField.setAttribute("name", "output_name");
    nameField.setAttribute("value", outputName);

    form.appendChild(dataField);
    form.appendChild(nameField);
    form.appendChild(document.getElementById('width'));
    form.appendChild(document.getElementById('height'));
    document.body.appendChild(form);     
    form.submit();
  }

</script>
